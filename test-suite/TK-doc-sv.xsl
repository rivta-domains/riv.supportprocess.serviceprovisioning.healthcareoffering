<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/testsuite">
		<html>
			<head>
				<style>
					ul {
						margin-top: 10px;
						margin-bottom: 10px;
					}
					h2, h3 {
						color: #196619;
					}
				</style>
			</head>
			<body>
				<h1>TK-testsvit för <xsl:value-of select="id"/></h1>
				
				<h2>Om testsviten</h2>
				<p>Detta dokument beskriver testsviten för <xsl:value-of select="id"/>. Testsviten innehåller ett antal testfall som kan användas för att verifiera implementationen innan integrationen med den nationella tjänsteplattformen.<br/>
				Testsviten använder SoapUI för att verifiera implementationen. Dokumentation om SoapUI hittas här: <a target="blank" href="https://www.soapui.org/getting-started/introduction.html">www.soapui.org</a>.<br/>
				Klicka på <a target="blank" href="https://www.soapui.org/downloads/soapui.html">den här länken</a> för att ladda hem en gratisversion av SoapUI. Installera enligt anvisning.</p>
				
				<h2>Förberedelser</h2>
				<p>
					<ul>
						<li>Gå till mappen <b>test-suite</b> i release-paketet</li>
						<li>Kopiera filen <b>‘soapui-support-N.N.NN.jar’</b> ('N.N.NN' är versionsnummer) till mappen <b>/bin/ext</b> där Soap-UI är installerat (leta efter mappen <b>/Program Files/Smartbear</b> på PC eller <b>/contents/java/app</b> på MAC OS)</li>
						<li>Öppna SoapUI och importera SoapUI-projektet (<b>test-suite/<xsl:value-of select="contractName"/>/<xsl:value-of select="contractName"/>-soapui-project.xml</b>) (välj ‘Import Project’ från menyn 'File')</li>
						<li>Om din webservice kräver ett SSL-certifikat, kan detta konfigureras via 'Preferences' (via menyn 'File').  
						I fönstret för inställningar, gå till fliken 'SSL Settings' och importera ditt certifikat under 'Keystore'</li>
						<li>Uppdatera <i>data.xml</i> så att den matchar den testdata du har i ditt system. Om du inte har testdata som passar så behöver detta läggas in i källsystemet (se nedan för instruktioner)</li>
						<li>Du borde nu kunna köra testfallen i SoapUI</li>
					</ul>
				</p>
				
				<h2>Testdata i <i>data.xml</i></h2>
				<p>
					Innan man kör testfallen i SoapUI så måste den data som skickas med i anropen anpassas utifrån det system som man vill testa. Detta görs genom att ändra i filen <i>data.xml</i> enligt nedan.<br/>
					<br/>
					Filen är i XML-format och i början finns en sektion som heter "globaldata". Här anger man den konfiguration som kommer att användas av alla testfall.<br/>
					Varje element i "globaldata" kan omdefinieras för ett specifikt testfall vid behov. Följande element är globala:
					<ul>
						<xsl:for-each select="globaldata/*">
							<li>
								<xsl:value-of select="name()"/>
							</li>
						</xsl:for-each>
					</ul>
					Globaldata innehåller ett antal konfigurationsparametrar för loggning: <br/>
                    <b>logTestData:</b> Sätts till true/false beroende på om loggning ska utföras eller ej. <b> Observera att patientdata kan lagras vid påslagen loggning.</b> <br/>
                    <b>logTestDataPath:</b> Sökvägen till den katalog där loggfilerna sparas, måste vara en katalog som användaren har rättighet att skriva i. <br/>
                    <b>logTestDataFilesAllowed:</b> Max antal filer som sparas. Det blir en fil för varje testfall som körs. När max antal filer har uppnåtts tas de äldsta bort så nya kan sparas.<br/>
				</p>
				
				<h2>Allmänna tips</h2>
				<p>
					<ul>
						<li>Kör ett testfall i taget och verifiera resultatet. Man kan också köra en hel testsvit (t.ex. "1 Basic tests") för att köra igenom alla testfall i just den sviten.<br/>
						Om du gör någon ändring som ska påverka ett specifikt testfall, kan man efter att ha verifierat just det testfallet köra genom hela sviten för att snabbt se att det "hänger ihop".</li>
						<li>Eventuella felmeddelanden skrivs ut dels till fönstret för testfallet och dels i sektionen "assertions" i anrops-fönstret.</li>
					</ul>
				</p>
				
				<h2>Beskrivning av testfallen</h2>
				<p>De parametrar man anger för ett specifikt testfall kompletterar och/eller omdefinierar de parametrar som anges i "globaldata".<br/>
				Det betyder att både parametrar från "globaldata" och det specifika testfallets sektion i filen används för det aktuella testfallet.<br/>
				OBS! Om en parameter med samma namn definieras både i "globaldata" och specifikt för testfallet, så kommer värdet från testfalls-sektionen att användas.<br/>
				Om man vill ändra värdet för en globalt definierad parameter i specifika testfall så kan parametern alltså läggas till i testfallet.<br/>
				Glöm inte att spara <i>data.xml</i> efter att du har ändrat i den.</p>
				<ul style="list-style-type:none">
					<xsl:for-each select="testcase|section">
						<h3>
							<li>
								<xsl:value-of select="@id"/>
							</li>
						</h3>
						<p>
							<xsl:choose>
								<xsl:when test="starts-with(@id, '1.1.1 ')">Verifierar att tjänsteproducenten kan returnera vårdtjänster, minst en av vardera fysisk och virtuell plats</xsl:when>
								<xsl:when test="starts-with(@id, '1.1.2 ')">Verifierar att tjänsteproducenten kan filtrera på fysisk plats.</xsl:when>
								<xsl:when test="starts-with(@id, '1.1.3 ')">Verifierar att tjänsteproducenten kan filtrera på virtuell plats.</xsl:when>
								<xsl:when test="starts-with(@id, '1.2 ')">Verifierar att tjänsteproducenten kan filtrera på vård- och omsorgstjänstens id.</xsl:when>
								<xsl:when test="starts-with(@id, '1.3 ')">Verifierar att tjänsteproducenten kan filtrera på typ av vård- och omsorgstjänst.</xsl:when>
								<xsl:when test="starts-with(@id, '1.4 ')">Verifierar att tjänsteproducenten kan filtrera på verksamhetskod.</xsl:when>
								<xsl:when test="starts-with(@id, '1.5 ')">Verifierar att tjänsteproducenten kan filtrera på utbudsansvarig organisation.</xsl:when>
								<xsl:when test="starts-with(@id, '1.6 ')">Verifierar att tjänsteproducenten kan filtrera så att endast poster där vård- och omsorgstjänst som erbjuds av en offentlig huvudman returneras.</xsl:when>
								<xsl:when test="starts-with(@id, '1.7.1 ')">Verifierar att tjänsteproducenten kan filtrera så att poster där <b>management.code = Region</b> returneras.</xsl:when>
								<xsl:when test="starts-with(@id, '1.7.2 ')">Verifierar att tjänsteproducenten kan filtrera så att poster där <b>management.code = Kommun</b> returneras.</xsl:when>
								<xsl:when test="starts-with(@id, '1.7.3 ')">Verifierar att tjänsteproducenten kan filtrera så att poster där <b>management.code = Statlig</b> returneras.</xsl:when>
								<xsl:when test="starts-with(@id, '1.7.4 ')">Verifierar att tjänsteproducenten kan filtrera så att poster där <b>management.code = Privat</b> returneras.</xsl:when>
								<xsl:when test="starts-with(@id, '1.7.5 ')">Verifierar att tjänsteproducenten kan filtrera så att poster där <b>management.code = Övrigt</b> returneras.</xsl:when>
								<xsl:when test="starts-with(@id, '1.8 ')">Verifierar att tjänsteproducenten kan filtrera på språk för aktör - om det saknas text för angivet språk, returneras texter på svenska.</xsl:when>
								<xsl:when test="starts-with(@id, '1.9.1 ')">Verifierar att tjänsteproducenten kan filtrera på roll för aktör, enskild person - om det saknas text för angiven roll, returneras texter anpassade för invånare.</xsl:when>
								<xsl:when test="starts-with(@id, '1.9.2 ')">Verifierar att tjänsteproducenten kan filtrera på roll för aktör, hälso- och sjukvårdspersonal - om det saknas text för angiven roll, returneras texter anpassade för invånare.</xsl:when>
								<xsl:when test="starts-with(@id, '1.10 ')">Verifierar att tjänsteproducenten kan filtrera på id på utförande enhet.</xsl:when>
								<xsl:when test="starts-with(@id, '1.11 ')">Verifierar att tjänsteproducenten kan filtrera på ålder för målgrupp.</xsl:when>
								<xsl:when test="starts-with(@id, '1.12.1 ')">Verifierar att tjänsteproducenten kan filtrera på kön för målgrupp, man.</xsl:when>
								<xsl:when test="starts-with(@id, '1.12.2 ')">Verifierar att tjänsteproducenten kan filtrera på kön för målgrupp, kvinna.</xsl:when>
								<xsl:when test="starts-with(@id, '1.13 ')">Verifierar att tjänsteproducenten kan filtrera på egenskap som erbjuds av tjänst..</xsl:when>
								<xsl:when test="starts-with(@id, '1.14.1 ')">Verifierar att tjänsteproducenten kan filtrera på sökområde Geografiskt område.</xsl:when>
								<xsl:when test="starts-with(@id, '1.14.2 ')">Verifierar att tjänsteproducenten kan filtrera på sökområde län.</xsl:when>
								<xsl:when test="starts-with(@id, '1.14.3 ')">Verifierar att tjänsteproducenten kan filtrera på sökområde komun.</xsl:when>
								<xsl:when test="starts-with(@id, '1.15 ')">Verifierar att tjänsteproducenten kan filtrera på fritext.</xsl:when>
								<xsl:when test="starts-with(@id, '1.16 ')">Verifierar att tjänsteproducenten kan returnera ett SoapFault vid fel. I detta fall skickas ett felaktigt anrop.</xsl:when>								
								<xsl:when test="starts-with(@id, '2.1 ')">Verifierar att
									<ul>
										<li>Header-attributet "Content-type" har, om attributet finns, en teckenuppsättning som är satt till UTF-8 eller UTF-16</li>
										<li>Attributet "XML Prolog" har, om attributet finns, en teckenuppsättning som är satt till UTF-8 eller UTF-16</li>
										<li>Om båda attributen finns så ska de vara lika</li>
									</ul></xsl:when>
								<xsl:when test="starts-with(@id, '2.2 ')">Verifierar att responsen innehåller en sträng med specialtecken.<br/>
									Denna sträng behöver läggas upp på en post i källsystemet och bör innehålla så många specialtecken som möjligt.</xsl:when>
								<xsl:when test="starts-with(@id, '5.1 ')">.</xsl:when>
								<xsl:when test="starts-with(@id, '5.2.1 ')">Verifierar att tjänsteproducenten kan returnera en post som talar om att informationen får delas till patient. Element <b>approvedForPatient</b> i headern.</xsl:when>
								<xsl:when test="starts-with(@id, '5.2.2 ')">Verifierar att tjänsteproducenten kan returnera en post som talar om att informationen inte får delas till patient. Element <b>approvedForPatient</b> i headern.</xsl:when>
								<xsl:when test="starts-with(@id, '5.2.3 ')">Verifierar att tjänsteproducenten kan returnera en signerad post. Element <b>signature</b> i headern.</xsl:when>
								<xsl:when test="starts-with(@id, '5.2.4 ')">Verifierar att tjänsteproducenten kan returnera en osignerad post. Element <b>signature</b> i headern.</xsl:when>
								<xsl:when test="starts-with(@id, '5.3.1 ')">Verifierar att tjänsteproducenten kan leverera <b>validity/start</b>.</xsl:when>
								<xsl:when test="starts-with(@id, '5.3.2 ')">Verifierar att tjänsteproducenten kan leverera <b>validity/end</b>.</xsl:when>
								<xsl:when test="starts-with(@id, '5.4.1 ')">Verifierar att tjänsteproducenten kan leverera status <b>INACTIVE</b>.</xsl:when>
								<xsl:when test="starts-with(@id, '5.4.2 ')">Verifierar att tjänsteproducenten kan leverera status <b>ACTIVE</b>.</xsl:when>
								<xsl:when test="starts-with(@id, '5.4.3 ')">Verifierar att tjänsteproducenten kan leverera status <b>DEPRECATED</b>.</xsl:when>
								<xsl:when test="starts-with(@id, '5.5.1 ')">erifierar att tjänsteproducenten kan leverera <b>careOption</b> med värde <b>true</b>.</xsl:when>
								<xsl:when test="starts-with(@id, '5.5.2 ')">Verifierar att tjänsteproducenten kan leverera <b>careOption</b> med värde <b>false</b>.</xsl:when>
								<xsl:when test="starts-with(@id, '5.6.1 ')">Verifierar att tjänsteproducenten kan leverera <b>careOption</b> med värde <b>true</b>.</xsl:when>
								<xsl:when test="starts-with(@id, '5.6.2 ')">Verifierar att tjänsteproducenten kan leverera <b>careOption</b> med värde <b>false</b>.</xsl:when>
								<xsl:when test="starts-with(@id, '5.7 ')">Verifierar att tjänsteproducenten kan leverera <b>beskrivning</b> av vård- och omsorgstjänsten.</xsl:when>
								<xsl:when test="starts-with(@id, '5.8 ')">Verifierar att tjänsteproducenten kan leverera <b>indicator</b>.</xsl:when>
								<xsl:when test="starts-with(@id, '5.9 ')">Verifierar att tjänsteproducenten kan leverera <b>remissmall</b>.</xsl:when>
								<xsl:when test="starts-with(@id, '5.10.1 ')">Verifierar att tjänsteproducenten kan leverera <b>versamhetskod</b>.</xsl:when>
								<xsl:when test="starts-with(@id, '5.10.2 ')">Verifierar att tjänsteproducenten kan leverera <b>information om öppettider</b>. OBS! Format måste verfierieras manuellt.</xsl:when>
								<xsl:when test="starts-with(@id, '5.12 ')">Verifierar att tjänsteproducenten kan leverera information om <b>patientavgift</b>.</xsl:when>
								<xsl:when test="starts-with(@id, '5.13.1 ')">Verifierar att tjänsteproducenten kan leverera information om <b>målgrupp: ålder</b>.</xsl:when>
								<xsl:when test="starts-with(@id, '5.13.2 ')">Verifierar att tjänsteproducenten kan leverera information om <b>målgrupp: kön</b>.</xsl:when>
								<xsl:when test="starts-with(@id, '5.13.3 ')">Verifierar att tjänsteproducenten kan leverera information om <b>målgrupp: attribut</b>.</xsl:when>
								<xsl:when test="starts-with(@id, '5.14.1 ')">Verifierar att tjänsteproducenten kan leverera information om <b>länskod</b>.</xsl:when>
								<xsl:when test="starts-with(@id, '5.14.2 ')">Verifierar att tjänsteproducenten kan leverera information om <b>kommunkod</b>.</xsl:when>
								<xsl:when test="starts-with(@id, '5.14.3 ')">Verifierar att tjänsteproducenten kan leverera information om <b>övriga områden (polygon)</b>.</xsl:when>
								<xsl:when test="starts-with(@id, '5.14.4 ')">Verifierar att tjänsteproducenten kan leverera information om <b>fysiskt plats</b>.</xsl:when>
								<xsl:when test="starts-with(@id, '5.14.5 ')">Verifierar att tjänsteproducenten kan leverera information om <b>virtuell plats</b>.</xsl:when>
								<xsl:when test="starts-with(@id, '5.15.1 ')">Verifierar att tjänsteproducenten kan leverera <b>kontaktuppgifter: postadress</b>.</xsl:when>
								<xsl:when test="starts-with(@id, '5.15.2 ')">Verifierar att tjänsteproducenten kan leverera <b>kontaktuppgifter: elektronisk adress</b>.</xsl:when>
								<xsl:when test="starts-with(@id, '5.16 ')">Verifierar att tjänsteproducenten kan leverera information om <b>samverkan</b>.</xsl:when>
								<xsl:when test="starts-with(@id, '5.17 ')">Verifierar att tjänsteproducenten kan leverera information om <b>resurs</b>.</xsl:when>
								<xsl:when test="starts-with(@id, '5.18 ')">Verifierar att tjänsteproducenten kan leverera <b>störningsinformation</b>.</xsl:when>															
								<xsl:when test="starts-with(@id, '6.1 Loadtest')"><b>6.1.1 Grund</b><br />
								Syftet med testet är dels att verifiera att systemet kan hantera minst 10 samtidiga trådar, dels att skapa sig en bild av systemets prestanda. Testet är designat att ta max 3 minuter.<br />
								I SLA-kapitlet i självdeklarationen, under "Övrig kommentar", ange genomsnittlig responstid som visas i sista raden i kolumn "avg" med enhet millisekunder (ms).
								<br /><br /><b>6.1.2 Uthållighet</b><br />
								Syftet med testet är att undersöka prestanda hos systemet över längre tid (30 minuter). I SLA-kapitlet i självdeklarationen, under "Övrig kommentar", notera om testet gick att genomföra utan problem. Om inte, notera hur lång tid det var möjligt att köra.	</xsl:when>
								<xsl:when test="starts-with(@id, '6.2 Recovery')"><b>6.2.1 Återhämtning</b><br />
								Syftet med testet är att utsätta systemet för maximal last och att verifiera att systemet automatiskt återhämtar sig. För att kontrollera att systemet har kunnat återhämta sig efter maximal last så rekommenderas att köra testfall "1.1.1 Personnummer" för att se att systemet svarar. I SLA-kapitlet i självdeklarationen, under "Övrig kommentar", notera om systemet kunde återhämta sig efter att ha utsatts för maximal last. Ange även hur många trådar som testet avslutades med.</xsl:when>
								<xsl:otherwise>
									<xsl:copy-of select="description"/>
								</xsl:otherwise>							
							</xsl:choose>						
						</p>
						<xsl:if test="data/*">
							<b>Testfalls-specifika parametrar</b>
						</xsl:if>
						<ul>
							<xsl:for-each select="data/*">
								<li>
									<xsl:value-of select="name()"/>
								</li>
							</xsl:for-each>
						</ul>
					</xsl:for-each>
				</ul>
				<br/>
				<br/>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet> 